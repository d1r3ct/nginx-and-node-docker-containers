FROM nginx

COPY html /usr/share/nginx/html

COPY images /usr/share/nginx/images

COPY nginx.conf /etc/nginx/nginx.conf

CMD ["nginx", "-g", "daemon off;"]
